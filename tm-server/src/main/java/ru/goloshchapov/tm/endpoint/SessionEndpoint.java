package ru.goloshchapov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.endpoint.ISessionEndpoint;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.dto.Result;
import ru.goloshchapov.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static ru.goloshchapov.tm.constant.ResultConst.RESULT_FAIL;
import static ru.goloshchapov.tm.constant.ResultConst.RESULT_SUCCESS;


@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    private final ServiceLocator serviceLocator;

    public SessionEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public @Nullable Session openSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @Override
    @WebMethod
    public @NotNull Result closeSession(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getSessionService().close(session);
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    public @NotNull Result closeSessionByLogin(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getSessionService().closeSessionByLogin(login, password);
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }
}
