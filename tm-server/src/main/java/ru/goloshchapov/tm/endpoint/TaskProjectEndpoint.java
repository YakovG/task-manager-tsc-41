package ru.goloshchapov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.dto.Project;
import ru.goloshchapov.tm.dto.Session;
import ru.goloshchapov.tm.dto.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskProjectEndpoint implements ru.goloshchapov.tm.api.endpoint.ITaskProjectEndpoint {

    private final ServiceLocator serviceLocator;

    public TaskProjectEndpoint (ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    @SneakyThrows
    @Nullable
    public List<Task> findAllTaskByProjectId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmptyProjectWithTaskById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().isEmptyProjectById(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Task> findAllTaskByProjectName(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectName") final String projectName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllByProjectName(session.getUserId(), projectName);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmptyProjectWithTaskByName(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectName") final String projectName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().isEmptyProjectByName(session.getUserId(), projectName);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Task> findAllTaskByProjectIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectIndex") final Integer projectIndex
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllByProjectIndex(session.getUserId(), projectIndex);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmptyProjectWithTaskByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectIndex") final Integer projectIndex
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().isEmptyProjectByIndex(session.getUserId(), projectIndex);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Task bindTaskToProjectById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String taskId,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().bindToProjectById(session.getUserId(), taskId, projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Task unbindTaskFromProjectById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String taskId,
            @WebParam(name = "projectId") String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().unbindFromProjectById(session.getUserId(), taskId, projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Task> removeAllTaskByProjectId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeAllByProjectId(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Task> removeAllTaskByProjectName(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectName") final String projectName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeAllByProjectName(session.getUserId(), projectName);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Task> removeAllTaskByProjectIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectIndex") final Integer projectIndex
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeAllByProjectIndex(session.getUserId(), projectIndex);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean removeAllTaskByUserId(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Project removeProjectByIdWithTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Project removeProjectByNameWithTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectName") final String projectName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectByName(session.getUserId(), projectName);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Project removeProjectByIndexWithTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectIndex") final Integer projectIndex
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().removeProjectByIndex(session.getUserId(), projectIndex);
    }

}
