package ru.goloshchapov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.endpoint.IAdminEndpoint;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.dto.*;
import ru.goloshchapov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

import static ru.goloshchapov.tm.constant.ResultConst.RESULT_FAIL;
import static ru.goloshchapov.tm.constant.ResultConst.RESULT_SUCCESS;

@WebService
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    private final ServiceLocator serviceLocator;

    public AdminEndpoint(ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;}

    @Override
    @WebMethod
    @SneakyThrows
    public void addUserAll(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "collection") final Collection<User> collection
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().addAll(collection);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User addUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "user") final User entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().add(entity);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable List<User> findUserAll(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User findUserById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "userId") final String id) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findOneById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User findUserByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findOneByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isUserAbsentById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "userId") final String id) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().isAbsentById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isUserAbsentByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().isAbsentByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public String getUserIdByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().getIdByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void showAllUserWithProject(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void showUserList(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public int sizeUser(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().size();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearUser(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User removeUserById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "userId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeOneById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User removeUserByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeOneByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isUserLoginExists(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().isLoginExists(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isEmailExists(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().isEmailExists(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User createUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User createUserWithEmail(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User createUserWithRole(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "role") final Role role
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User createUserWithAll(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email,
            @WebParam(name = "role") final String role
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, email, role);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User findUserByLogin(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User findUserByEmail(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "email") final String email) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findUserByEmail(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable User removeUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "user") final User user) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUser(user);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User removeUserByLogin(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUserByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User removeUserByEmail(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "email") final String email) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUserByEmail(email);
    }


    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User lockUserByLogin(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User unlockUserByLogin(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") final String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User lockUserByEmail(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockUserByEmail(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull User unlockUserByEmail(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unlockUserByEmail(email);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectByNameWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findOneByName(name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isProjectAbsentByNameWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().isAbsentByName(name);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public String getProjectIdByNameWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name ") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().getIdByName(name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void addProjectAllWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "collection") Collection<Project> collection
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getProjectService().addAll(collection);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Project addProjectWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "project") final Project entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().add(entity);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Project> findProjectAllWithoutUserId(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectByIdWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findOneById(id);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectByIndexWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findOneByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isProjectAbsentByIdWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().isAbsentById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isProjectAbsentByIndexWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().isAbsentByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public String getProjectIdByIndexWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().getIdByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public int sizeProjectWithoutUserId(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().size();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearProjectWithoutUserId(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getProjectService().clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeProjectWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "project") final Project entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getProjectService().remove(entity);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @Nullable
    public Project removeProjectByIdWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().removeOneById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    @Nullable
    public Project removeProjectByIndexWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().removeOneByIndex(index);
    }


    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskByNameWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findOneByName(name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isTaskAbsentByNameWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().isAbsentByName(name);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public String getTaskIdByNameWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().getIdByName(name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void addTaskAllWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "collection") Collection<Task> collection
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getTaskService().addAll(collection);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Task addTaskWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") final Task entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().add(entity);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable List<Task> findTaskAllWithoutUserId(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskByIdWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findOneById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskByIndexWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findOneByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isTaskAbsentByIdWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().isAbsentById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean isTaskAbsentByIndexWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().isAbsentByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public String getTaskIdByIndexWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().getIdByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public int sizeTaskWithoutUserId(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().size();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearTaskWithoutUserId(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getTaskService().clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeTaskWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") final Task entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getTaskService().remove(entity);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable Task removeTaskByIdWithoutUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().removeOneById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result saveBase64Data(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().saveBase64Data();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result loadBase64Data(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().loadBase64Data();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result saveBinaryData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().saveBinaryData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result loadBinaryData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().loadBinaryData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result saveJsonFasterXmlData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().saveDataJsonFasterXml();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result loadJsonFasterXmlData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().loadDataJsonFasterXml();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result saveJsonJaxBData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().saveJsonJaxBData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result loadJsonJaxBData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().loadJsonJaxBData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result saveXmlFasterXmlData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().saveXmlFasterXmlData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result loadXmlFasterXmlData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().loadXmlFasterXmlData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result saveXmlJaxBData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().saveXmlJaxBData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result loadXmlJaxBData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().loadXmlJaxBData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result saveYamlData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().saveYamlData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result loadYamlData(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().loadYamlData();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result saveBackup(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().saveBackup();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result loadBackup(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().loadBackup();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull Result clearBackup(
            @WebParam(name = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final Result result = new Result();
        try {
            serviceLocator.getDataService().clearBackup();
            result.setResult(RESULT_SUCCESS);
        }
        catch (Exception e) {
            result.setResult(RESULT_FAIL);
        }
        return result;
    }
}
