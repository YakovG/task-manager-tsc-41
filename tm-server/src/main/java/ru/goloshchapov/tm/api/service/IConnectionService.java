package ru.goloshchapov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull SqlSession getSqlSession();

    SqlSessionFactory getSqlSessionFactory();
}
