package ru.goloshchapov.tm.api.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO `tm_session`(`id`,`timestamp`,`userId`,`signature`) " +
            "VALUES(#{id}, #{timestamp}, #{userId}, #{signature})")
    void add (@NotNull Session session);

    @Select("SELECT COUNT(*) FROM `tm_session` WHERE `id` = #{sessionId}")
    int contains(@Nullable String sessionId);

    @Select("DELETE FROM `tm_session` WHERE `id` = #{sessionId}")
    void remove(@NotNull String sessionId);

    @Nullable
    @Select("SELECT * FROM `tm_session` WHERE `userId` = #{userId} LIMIT 1")
    Session findByUserId (@Nullable String userId);

    @Nullable
    @Select("SELECT * FROM `tm_session`")
    List<Session> findAllSession();

    @Delete("DELETE FROM `tm_session`")
    void clear();

    @Select("SELECT * FROM `tm_session` WHERE `id` = #{id} LIMIT 1")
    Session findSessionById(@Nullable final String id);
}
