package ru.goloshchapov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.dto.Project;
import ru.goloshchapov.tm.dto.Session;
import ru.goloshchapov.tm.dto.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskProjectEndpoint {
    @WebMethod
    @SneakyThrows
    @Nullable List<Task> findAllTaskByProjectId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String projectId
    );

    @WebMethod
    @SneakyThrows
    boolean isEmptyProjectWithTaskById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String projectId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<Task> findAllTaskByProjectName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectName") String projectName
    );

    @WebMethod
    @SneakyThrows
    boolean isEmptyProjectWithTaskByName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectName") String projectName
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<Task> findAllTaskByProjectIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectIndex") Integer projectIndex
    );

    @WebMethod
    @SneakyThrows
    boolean isEmptyProjectWithTaskByIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectIndex") Integer projectIndex
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Task bindTaskToProjectById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String taskId,
            @WebParam(name = "projectId") String projectId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    Task unbindTaskFromProjectById(
            @WebParam(name = "session") Session session,
            @WebParam(name = "taskId") String taskId,
            @WebParam(name = "projectId") String projectId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<Task> removeAllTaskByProjectId(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String projectId
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<Task> removeAllTaskByProjectName(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectName") String projectName
    );

    @Nullable
    @WebMethod
    @SneakyThrows
    List<Task> removeAllTaskByProjectIndex(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectIndex") Integer projectIndex
    );

    @WebMethod
    @SneakyThrows
    boolean removeAllTaskByUserId(
            @WebParam(name = "session") Session session
    );

    @WebMethod
    @SneakyThrows
    @Nullable Project removeProjectByIdWithTask(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectId") String projectId
    );

    @WebMethod
    @SneakyThrows
    @Nullable Project removeProjectByNameWithTask(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectName") String projectName
    );

    @WebMethod
    @SneakyThrows
    @Nullable Project removeProjectByIndexWithTask(
            @WebParam(name = "session") Session session,
            @WebParam(name = "projectIndex") Integer projectIndex
    );
}
