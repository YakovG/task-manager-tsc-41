package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tm_user")
@NoArgsConstructor
public class User extends AbstractEntity{

    @Column
    @NotNull
    private String login;

    @Column
    @NotNull
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstname;

    @Column
    @Nullable
    private String lastname;

    @Column
    @Nullable
    private String middlename;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column
    private boolean locked = false;

}
