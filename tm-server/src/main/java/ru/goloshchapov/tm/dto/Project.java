package ru.goloshchapov.tm.dto;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tm_project")
@NoArgsConstructor
public class Project extends AbstractBusinessEntity {

}
