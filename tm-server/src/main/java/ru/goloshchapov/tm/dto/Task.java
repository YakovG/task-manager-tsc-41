package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
@NoArgsConstructor
public class Task extends AbstractBusinessEntity {

    @Column
    @Nullable
    private String projectId = null;

}