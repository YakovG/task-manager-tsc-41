package ru.goloshchapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tm_session")
@NoArgsConstructor
public class Session extends AbstractEntity implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Getter
    @Setter
    private Long timestamp;

    @Getter
    @Setter
    private String userId;

    @Getter
    @Setter
    private String signature;
    
}
