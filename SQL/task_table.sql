CREATE TABLE `tm_task` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`description` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`userId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`projectId` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`status` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`created` DATETIME NULL DEFAULT NULL,
	`dateStart` DATETIME NULL DEFAULT NULL,
	`dateFinish` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `FK_tm_task_tm_user` (`userId`) USING BTREE,
	INDEX `FK_tm_task_tm_project` (`projectId`) USING BTREE,
	CONSTRAINT `FK_tm_task_tm_project` FOREIGN KEY (`projectId`) REFERENCES `task-manager`.`tm_project` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT,
	CONSTRAINT `FK_tm_task_tm_user` FOREIGN KEY (`userId`) REFERENCES `task-manager`.`tm_user` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

