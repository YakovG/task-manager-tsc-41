package ru.goloshchapov.tm.endpoint;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.4.3
 * 2021-05-30T18:57:48.709+03:00
 * Generated source version: 3.4.3
 *
 */
@WebServiceClient(name = "TaskProjectEndpointService",
                  wsdlLocation = "http://0.0.0.0:8080/TaskProjectEndpoint?WSDL",
                  targetNamespace = "http://endpoint.tm.goloshchapov.ru/")
public class TaskProjectEndpointService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://endpoint.tm.goloshchapov.ru/", "TaskProjectEndpointService");
    public final static QName TaskProjectEndpointPort = new QName("http://endpoint.tm.goloshchapov.ru/", "TaskProjectEndpointPort");
    static {
        URL url = null;
        try {
            url = new URL("http://0.0.0.0:8080/TaskProjectEndpoint?WSDL");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(TaskProjectEndpointService.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "http://0.0.0.0:8080/TaskProjectEndpoint?WSDL");
        }
        WSDL_LOCATION = url;
    }

    public TaskProjectEndpointService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public TaskProjectEndpointService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public TaskProjectEndpointService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public TaskProjectEndpointService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public TaskProjectEndpointService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public TaskProjectEndpointService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns TaskProjectEndpoint
     */
    @WebEndpoint(name = "TaskProjectEndpointPort")
    public TaskProjectEndpoint getTaskProjectEndpointPort() {
        return super.getPort(TaskProjectEndpointPort, TaskProjectEndpoint.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns TaskProjectEndpoint
     */
    @WebEndpoint(name = "TaskProjectEndpointPort")
    public TaskProjectEndpoint getTaskProjectEndpointPort(WebServiceFeature... features) {
        return super.getPort(TaskProjectEndpointPort, TaskProjectEndpoint.class, features);
    }

}
