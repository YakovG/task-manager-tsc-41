package ru.goloshchapov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.3
 * 2021-05-30T18:57:48.787+03:00
 * Generated source version: 3.4.3
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.goloshchapov.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/UserEndpoint/updateUserRequest", output = "http://endpoint.tm.goloshchapov.ru/UserEndpoint/updateUserResponse")
    @RequestWrapper(localName = "updateUser", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.UpdateUser")
    @ResponseWrapper(localName = "updateUserResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.UpdateUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.goloshchapov.tm.endpoint.User updateUser(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName,
        @WebParam(name = "middleName", targetNamespace = "")
        java.lang.String middleName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/UserEndpoint/getUserRequest", output = "http://endpoint.tm.goloshchapov.ru/UserEndpoint/getUserResponse")
    @RequestWrapper(localName = "getUser", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.GetUser")
    @ResponseWrapper(localName = "getUserResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.GetUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.goloshchapov.tm.endpoint.User getUser(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.goloshchapov.ru/UserEndpoint/setUserPasswordRequest", output = "http://endpoint.tm.goloshchapov.ru/UserEndpoint/setUserPasswordResponse")
    @RequestWrapper(localName = "setUserPassword", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.SetUserPassword")
    @ResponseWrapper(localName = "setUserPasswordResponse", targetNamespace = "http://endpoint.tm.goloshchapov.ru/", className = "ru.goloshchapov.tm.endpoint.SetUserPasswordResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.goloshchapov.tm.endpoint.User setUserPassword(

        @WebParam(name = "session", targetNamespace = "")
        ru.goloshchapov.tm.endpoint.Session session,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );
}
