package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.Task;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectIdListCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-list-by-project-id";

    @NotNull public static final String DESCRIPTION = "Show task list by project id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = endpointLocator.getTaskProjectEndpoint().findAllTaskByProjectId(session, projectId);
        if (tasks == null) throw new TaskNotFoundException();
        int index = 1;
        for (@NotNull final Task task: tasks) {
            System.out.println(index + ". " + task.getId() + " : " + task.getName());
            index++;
        }
    }
}
