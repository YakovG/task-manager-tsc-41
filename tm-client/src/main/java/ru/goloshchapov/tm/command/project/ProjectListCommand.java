package ru.goloshchapov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Project;
import ru.goloshchapov.tm.endpoint.Session;

import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-list";

    @NotNull public static final String DESCRIPTION = "Show project list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[PROJECT LIST]");
        @NotNull final List<Project> projects = endpointLocator.getProjectEndpoint().findAllProjectByUserId(session);
        int index = 1;
        for (@NotNull final Project project: projects) {
            System.out.println(index + ". " + project.getId() + " : " + project.getName());
            index++;
        }
    }
}
