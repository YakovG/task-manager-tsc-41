package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.Task;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.exception.entity.TaskNotUpdatedException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByIdUpdateCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-update-by-id";

    @NotNull public static final String DESCRIPTION = "Update task by id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().findTaskById(session, taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = endpointLocator.getTaskEndpoint()
                .updateTaskById(session, taskId,name,description);
        if (taskUpdated == null) throw new TaskNotUpdatedException();
    }
}
