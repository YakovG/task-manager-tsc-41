package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.Task;
import ru.goloshchapov.tm.exception.entity.TaskNotCreatedException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-create";

    @NotNull public static final String DESCRIPTION = "Create new task";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().addTaskByName(session, name, description);
        if (task == null) throw new TaskNotCreatedException();
    }
}
