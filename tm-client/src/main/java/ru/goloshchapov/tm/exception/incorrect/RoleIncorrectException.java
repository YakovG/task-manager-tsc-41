package ru.goloshchapov.tm.exception.incorrect;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.AbstractException;

public class RoleIncorrectException extends AbstractException {

    public RoleIncorrectException(@Nullable final String message) {
        super("Error! Role " + message + " is incorrect...");
    }

}
